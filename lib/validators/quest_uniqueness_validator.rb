# encoding: UTF-8
class QuestUniquenessValidator < ActiveModel::Validator
  def validate(record)
    date = record.created_at || Date.today
    
    count = Quest.where( "created_at >= ? AND created_at <= ? AND room_id = ? AND period_id = ? AND solved=false", date.beginning_of_day, date.end_of_day, record.room_id, record.period_id).count

    if count > (record.persisted? ? 1 : 0)
      record.errors[:room] << ": Für diese Stunde gibt es in dem Raum bereits ein offenes Hilfegesuch."
    end
  end
end
