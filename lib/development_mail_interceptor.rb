class DevelopmentMailInterceptor
  def self.delivering_email(message)
    message.subject = "DEV #{message.to} #{message.subject}"
    message.to = "kuehnert@marienschule.com"
  end
end
