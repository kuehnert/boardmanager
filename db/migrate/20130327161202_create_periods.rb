class CreatePeriods < ActiveRecord::Migration
  def change
    create_table :periods do |t|
      t.integer :number
      t.integer :day
      t.time :start
      t.time :finish
    end
  end
end
