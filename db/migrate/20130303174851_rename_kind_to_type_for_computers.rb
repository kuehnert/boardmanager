class RenameKindToTypeForComputers < ActiveRecord::Migration
  def up
    rename_column :computers, :type, :kind
  end

  def down
    rename_column :computers, :kind, :type
  end
end
