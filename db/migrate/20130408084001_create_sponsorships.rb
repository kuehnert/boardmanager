class CreateSponsorships < ActiveRecord::Migration
  def change
    create_table :sponsorships do |t|
      t.references :user
      t.references :room

      t.timestamps
    end
    add_index :sponsorships, :user_id
    add_index :sponsorships, :room_id
  end
end
