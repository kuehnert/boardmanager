class CreateBoards < ActiveRecord::Migration
  def change
    create_table :boards do |t|
      t.references :room
      t.string :serial
      t.date :purchased_on
      t.string :note

      t.timestamps
    end
    add_index :boards, :room_id
  end
end
