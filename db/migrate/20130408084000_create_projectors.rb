class CreateProjectors < ActiveRecord::Migration
  def change
    create_table :projectors do |t|
      t.references :room
      t.string :manufacturer, default: 'Promethean'
      t.string :model
      t.string :serial, length: 15
      t.string :resolution, length: 9, default: '1280x800'

      t.timestamps
    end
    add_index :projectors, :room_id
  end
end
