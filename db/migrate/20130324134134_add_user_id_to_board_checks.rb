class AddUserIdToBoardChecks < ActiveRecord::Migration
  def up
    add_column :boardchecks, :user_id, :integer
    remove_column :boardchecks, :knight
  end

  def down
    remove_column :boardchecks, :user_id
    add_column :boardchecks, :knight, :string
  end
end
