class AddImageAlignedToBoardchecks < ActiveRecord::Migration
  def up
    add_column :boardchecks, :image_aligned, :boolean, default: false
    remove_column :boardchecks, :board_id
  end
  
  def down
    remove_column :boardchecks, :image_aligned
    add_column :boardchecks, :board_id, :integer
  end
end
