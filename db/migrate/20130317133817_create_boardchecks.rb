class CreateBoardchecks < ActiveRecord::Migration
  def change
    create_table :boardchecks do |t|
      t.references :board
      t.references :room
      t.string :user
      t.boolean :undamaged
      t.boolean :timetable_present
      t.boolean :speakers_on
      t.boolean :speakers_volume_set
      t.boolean :projector_remote_ok
      t.boolean :projector_source_set
      t.boolean :keyboard_ok
      t.boolean :teacher_pen_ok
      t.boolean :student_pen_ok
      t.boolean :sharpness_set
      t.integer :computer_state
      t.boolean :guest_account_ok
      t.boolean :network_accounts_ok
      t.boolean :activdriver_ok
      t.boolean :calibrated
      t.boolean :speakers_driver_ok
      t.boolean :updates_loaded
      t.boolean :issues_present
      t.text :note

      t.timestamps
    end
    add_index :boardchecks, :board_id
    add_index :boardchecks, :room_id
  end
end
