class CreateDuties < ActiveRecord::Migration
  def change
    create_table :duties do |t|
      t.references :user
      t.references :period
      t.string :room, length: 12
    end

    add_index :duties, :user_id
    add_index :duties, :period_id
  end
end
