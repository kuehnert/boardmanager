class AddTwitterNameToUsers < ActiveRecord::Migration
  def up
    add_column :users, :twitter_name, :string, length: 15
  end
  
  def down
    remove_column :users, :twitter_name
  end
end
