class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.string :name
      t.integer :building
      t.integer :level

      t.timestamps
    end
  end
end
