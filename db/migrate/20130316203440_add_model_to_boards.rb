class AddModelToBoards < ActiveRecord::Migration
  def change
    add_column :boards, :manufacturer, :string, default: "Promethean"
    add_column :boards, :model, :string
    
    Board.reset_column_information
    
    Board.update_all( manufacturer: "Promethean" )
  end
end
