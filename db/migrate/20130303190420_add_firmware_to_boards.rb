class AddFirmwareToBoards < ActiveRecord::Migration
  def change
    add_column :boards, :firmware, :string
    change_column :boards, :note, :text
  end
end