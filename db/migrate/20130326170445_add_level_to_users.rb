class AddLevelToUsers < ActiveRecord::Migration
  def up
    add_column :users, :level, :integer, default: 0

    User.reset_column_information
    User.where( admin: true ).update_all( level: (User::LEVELS.size - 1) )

    remove_column :users, :admin
  end
  
  
  def down
    add_column :users, :admin, :boolean, default: false

    User.reset_column_information
    User.where( level: 100 ).update_all( admin: true )

    remove_column :users, :level
  end
end
