class CreateQuests < ActiveRecord::Migration
  def change
    create_table :quests do |t|
      t.references :room
      t.references :period
      t.references :user
      t.string :problem
      t.text :note
      t.boolean :solved, default: false

      t.timestamps
    end
    add_index :quests, :room_id
    add_index :quests, :period_id
    add_index :quests, :user_id
  end
end
