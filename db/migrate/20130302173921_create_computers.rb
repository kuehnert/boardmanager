class CreateComputers < ActiveRecord::Migration
  def change
    create_table :computers do |t|
      t.references :room
      t.string :model
      t.string :type
      t.string :mac_wifi_address
      t.string :mac_en_address
      t.string :serial
      t.date :purchased_on
      t.text :note

      t.timestamps
    end
    add_index :computers, :room_id
  end
end
