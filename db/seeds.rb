# encoding: UTF-8

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

unless User.find_by_email('kuehnert@marienschule.com')
  admin = User.create(email: 'kuehnert@marienschule.com', firstname: 'Matthias', lastname: 'Kühnert', password: 'Winchester' )
  admin.update_attribute :level, User::LEVELS.size-1  
end

times = [
  [ Time.local(2000, 1, 1,  8, 00), Time.local(2000, 1, 1,  8, 45) ],
  [ Time.local(2000, 1, 1,  8, 45), Time.local(2000, 1, 1,  9, 35) ],
  [ Time.local(2000, 1, 1,  9, 35), Time.local(2000, 1, 1, 10, 20) ],
  [ Time.local(2000, 1, 1, 10, 20), Time.local(2000, 1, 1, 11, 25) ],
  [ Time.local(2000, 1, 1, 11, 25), Time.local(2000, 1, 1, 12, 10) ],
  [ Time.local(2000, 1, 1, 12, 10), Time.local(2000, 1, 1, 13, 10) ],
  [ Time.local(2000, 1, 1, 13, 10), Time.local(2000, 1, 1, 14, 00) ],
  [ Time.local(2000, 1, 1, 14, 00), Time.local(2000, 1, 1, 15, 00) ]
]

1.upto(5) do |day|
  times.size.times do |p|
    Period.create day: day, number: p, start: times[p][0], finish: times[p][1]
  end
end
