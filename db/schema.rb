# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130408084001) do

  create_table "boardchecks", :force => true do |t|
    t.integer  "room_id"
    t.boolean  "undamaged"
    t.boolean  "timetable_present"
    t.boolean  "speakers_on"
    t.boolean  "speakers_volume_set"
    t.boolean  "projector_remote_ok"
    t.boolean  "projector_source_set"
    t.boolean  "keyboard_ok"
    t.boolean  "teacher_pen_ok"
    t.boolean  "student_pen_ok"
    t.boolean  "sharpness_set"
    t.integer  "computer_state"
    t.boolean  "guest_account_ok"
    t.boolean  "network_accounts_ok"
    t.boolean  "activdriver_ok"
    t.boolean  "calibrated"
    t.boolean  "speakers_driver_ok"
    t.boolean  "updates_loaded"
    t.boolean  "issues_present"
    t.text     "note"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.integer  "user_id"
    t.boolean  "image_aligned",        :default => false
  end

  add_index "boardchecks", ["room_id"], :name => "index_boardchecks_on_room_id"

  create_table "boards", :force => true do |t|
    t.integer  "room_id"
    t.string   "serial"
    t.date     "purchased_on"
    t.text     "note"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "firmware"
    t.string   "manufacturer", :default => "Promethean"
    t.string   "model"
  end

  add_index "boards", ["room_id"], :name => "index_boards_on_room_id"

  create_table "computers", :force => true do |t|
    t.integer  "room_id"
    t.string   "model"
    t.string   "kind"
    t.string   "mac_wifi_address"
    t.string   "mac_en_address"
    t.string   "serial"
    t.date     "purchased_on"
    t.text     "note"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.string   "ip"
  end

  create_table "duties", :force => true do |t|
    t.integer "user_id"
    t.integer "period_id"
    t.string  "room"
  end

  add_index "duties", ["period_id"], :name => "index_duties_on_period_id"
  add_index "duties", ["user_id"], :name => "index_duties_on_user_id"

  create_table "periods", :force => true do |t|
    t.integer "number"
    t.integer "day"
    t.time    "start"
    t.time    "finish"
  end

  create_table "projectors", :force => true do |t|
    t.integer  "room_id"
    t.string   "manufacturer", :default => "Promethean"
    t.string   "model"
    t.string   "serial"
    t.string   "resolution",   :default => "1280x800"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "projectors", ["room_id"], :name => "index_projectors_on_room_id"

  create_table "quests", :force => true do |t|
    t.integer  "room_id"
    t.integer  "period_id"
    t.integer  "user_id"
    t.string   "problem"
    t.text     "note"
    t.boolean  "solved",     :default => false
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  add_index "quests", ["period_id"], :name => "index_quests_on_period_id"
  add_index "quests", ["room_id"], :name => "index_quests_on_room_id"
  add_index "quests", ["user_id"], :name => "index_quests_on_user_id"

  create_table "rooms", :force => true do |t|
    t.string   "name"
    t.integer  "building"
    t.integer  "level"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "sponsorships", :force => true do |t|
    t.integer  "user_id"
    t.integer  "room_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sponsorships", ["room_id"], :name => "index_sponsorships_on_room_id"
  add_index "sponsorships", ["user_id"], :name => "index_sponsorships_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "firstname",              :default => "", :null => false
    t.string   "lastname",               :default => "", :null => false
    t.integer  "year"
    t.string   "form"
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "authentication_token"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "unconfirmed_email"
    t.integer  "level",                  :default => 0
    t.integer  "failed_attempts",        :default => 0
    t.string   "twitter_name"
  end

  add_index "users", ["authentication_token"], :name => "index_users_on_authentication_token", :unique => true
  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["unlock_token"], :name => "index_users_on_unlock_token", :unique => true

end
