# encoding: UTF-8

module ApplicationHelper
  def markdown(text, options = {})
    markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML,
      autolink: true, space_after_headers: true, hard_wrap: true)
    markdown.render(text).html_safe
  end

  def format_date(date, format = :my)
    date.present? ? l(date.to_date, format: format) : '-'
  end
  
  def format_tick(value)
    value ? "✔" : "✘"
  end

  def format_check_cell(model, attribute)
    value = model.send(attribute)
    "<tr class=#{value ? 'success' : 'error'}>
    <td> #{model.class.human_attribute_name(attribute)}</td>
    <td class='center'> #{format_tick @boardcheck.send(attribute)}</td></tr>".html_safe
  end
  
  def format_day(i)
    Period::DAYS[i]
  end

  def format_number(n)
    n.to_i == 0 ? '-' : n
  end
  
  def array_to_hash(array)
    array.map.with_index{ |o,i| [o, i] }
  end

  def format_boardcheck_class(room)
    return nil unless room
    
    return case room.last_check_ok?
      when nil
        ''
      when true
        'success' 
      else
        'error'
    end
  end
  
  def format_duty_class(number)
    case number
    when 0
      'fail'
    when 1, 2
      'warn'
    else
      'ok'
    end
  end
  
  def format_quest_class(quest)
    if quest.solved?
      'success'
    elsif quest.user.blank?
      'error'
    else
      'warning'
    end
  end
  
  def format_email(user)
"mailto:#{user.email}?subject=Elektronische%20Tafeln&body=Gott%20zum%20Gru%C3%9Fe%2C%20#{user.level_s}%20#{user.firstname}%21%0A%0A%0A%0AAdieu%2C%0A#{current_user.level_s}%20#{current_user.firstname}"
  end
  
  def format_twitter(twitter_name)
    twitter_name.present? ? link_to(@user.twitter_name, "https://www.twitter.com/#{@user.twitter_name}", target: :new) : '✘'
  end
  
  def tweet_quest(quest, message)
    if Rails.env.production?
      Twitter.update "#{message} #{quest_url(quest)}"
    else
      # Hack to avoid Twitter timestamp problems
      Timecop.return 
      Twitter.update "#{message} #{quest_url quest, host: 'cray.local'}"
      Timecop.travel( Time.parse( BoardManager::Config.time ) )
    end
  end
end
