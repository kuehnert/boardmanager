module UsersHelper
  def format_name(user)
    s = "#{user.firstname} #{user.lastname}"
    s += " (#{user.yearform})" if user.year.present?
    return s
  end

  def format_title_name(user)
    "#{user.level_s} #{format_name(user)}"
  end
end
