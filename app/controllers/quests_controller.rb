# encoding: UTF-8
class QuestsController < InheritedResources::Base
  filter_resource_access
  layout 'minimal', only: :new
  
  def new
    @quest        = Quest.new
    @quest.period = Period.current
    @room         = @quest.room   = Room.find_by_name params[:room]
    
    if @room.present?
      if @quest.period
        @duties       = @quest.period.duties.includes(:user).ordered
        @duties.sort_by! { |d| @quest.room.distance(d.room) } if @quest.room.present?
      end
    else
      render 'locate'
    end
  end
  
  def create
    @quest = Quest.new( params[:quest] )

    if @quest.valid?
      @quest.save
      
      if BoardManager::Config.twitter['enabled']
        names   = @quest.period.users.twitterers.map { |u| "@#{u.twitter_name}"}.join(', ')
        message = "Tafelritterruf für #{names} in #{@quest.room.name}!"
        begin
          view_context.tweet_quest @quest, message
          flash[:notice] = "OK, Hilfe kommt!"
        rescue 
          flash[:error] = "Fehler beim Senden des Gesuchs; bitte Tafelritter holen"
        end
      else
        flash[:notice] = "OK, Euer Gesuch wurde versandt!"
      end
      
      redirect_to root_url
    else
      flash[:alert] = @quest.errors.full_messages.join(', ')
      render :new
    end
  end

  def accept
    @quest.user = current_user

    if @quest.save
      others = @quest.period.users.twitterers - [@quest.user]

      if BoardManager::Config.twitter['enabled'] && others.present?
        names = (@quest.period.users.twitterers - [@quest.user]).map { |u| "@#{u.twitter_name}"}.join(', ')
        message = "Erledigter Ruf in #{@quest.room.name}: #{@quest.user.titled} hat die Herausforderung angenommen, #{names}!"
        view_context.tweet_quest @quest, message
      end
    
      redirect_to edit_quest_path(@quest)
    else
      flash[:alert] = "Fehler beim Akzeptieren des Gesuchs"
      redirect_to quests_path
    end
  end
  
  protected
  def collection
    @quests ||= end_of_association_chain.ordered
  end

  def set_active_item
    @active = :quests
  end
end
