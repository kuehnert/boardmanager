class KnighttablesController < ApplicationController
  filter_access_to :all

  def show
    max_knights = BoardManager::Config.knighttable_max_duties
    @knighttable = Array.new( 8 ) { Array.new(6) { [] } }
    @duties = Duty.includes(:period, :user).order("users.year DESC").all
    @duties.map { |d| @knighttable[ d.period.number ][ d.period.day ] << d }

    if (params[:roomname].present?) and (@room = Room.named( params[:roomname] ))
      @knighttable.map { |r| r.map! { |duties| duties.sort_by { |d| @room.distance(d.room) }.first(max_knights) } }
    else
      @knighttable.map { |r| r.map! { |duties| duties.sort_by { rand }.first(max_knights) } }
    end
  end

  protected
    def set_active_item
      @active = :table
    end
end
