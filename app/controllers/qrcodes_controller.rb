class QrcodesController < ApplicationController
  def show
    url = params[:url]
    
    respond_to do |format|
      format.html
      format.svg  { render :qrcode => url, level: :h, unit: 4, offset: 10 }
      format.png  { render :qrcode => url }
      format.gif  { render :qrcode => url }
      format.jpeg { render :qrcode => url }
    end
  end
end
