class ComputersController < InheritedResources::Base
  filter_resource_access
  
  protected
    def collection
      @computers ||= end_of_association_chain.includes(:room).order('rooms.name')
    end
end
