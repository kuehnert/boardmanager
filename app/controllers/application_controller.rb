# encoding: UTF-8
require "application_responder"

class ApplicationController < ActionController::Base
  self.responder = ApplicationResponder
  respond_to :html

  protect_from_forgery
  helper :all
  before_filter :no_duties_warning, :set_active_item

  def set_current_user
    Authorization.current_user = current_user
  end

  def permission_denied
    flash[:error] = 'Du hast nicht die erforderlichen Rechte, diese Seite aufzurufen.'
    redirect_to :back rescue redirect_to root_url
  end
  
  protected
    def no_duties_warning
      if current_user && !session[:duties_warning_shown] && [1..3].include?(current_user.level) && current_user.duties.empty?
        flash.now[:info] = "Bitte trage #{view_context.link_to 'Bereitschaftstunden »', edit_all_user_duties_url(current_user), class: 'btn'} ein, in denen Du als Tafelritter zuhilfe gerufen werden kannst."
        session[:duties_warning_shown] = true
      end
    end
    
    def set_active_item
      @active = request.parameters[:controller] =~ /devise/ ? :user : nil
    end
end
