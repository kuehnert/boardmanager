class DutiesController < ApplicationController
  filter_resource_access :nested_in => :user, attribute_check: true, collection: [:index, :edit_all, :update_all]
  
  def index
    load_duties
  end

  def edit_all
    load_duties
  end

  def update_all
    @user = User.find params[:user_id]
    @user.duties.destroy_all
    periods = Period.all
    
    params[:duties].each_pair do |lesson, room|
      no, day = lesson.split('|')
      period  = periods.find { |p| p.day == day.to_i && p.number == no.to_i }
      room    = room.strip.sub( /^(?=\d)/, "R")
      @user.duties.create period_id: period.id, room: room
    end
    
    redirect_to user_duties_path(@user)
  end
  
  protected
    def load_duties
      @user = User.includes(:duties).find params[:user_id]
      @timetable = Array.new( 8 ) { Array.new(6, nil) }
      @duties = @user.duties.ordered
      @duties.each { |d| @timetable[ d.period.number ][ d.period.day ] = d.room }

      @timetable_stats = Array.new( 8 ) { Array.new(6, 0) }
      Duty.all.each { |d| @timetable_stats[ d.period.number ][ d.period.day ] += 1 }
    end

    def set_active_item
      @active = current_user.id == params[:user_id].to_i ? :user : :users
    end
end
