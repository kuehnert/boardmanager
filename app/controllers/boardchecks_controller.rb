class BoardchecksController < InheritedResources::Base
  filter_resource_access

  def new
    @boardcheck = Boardcheck.new( room_id: params[:room_id] )
  end
  
  protected
    def collection
      @boardchecks ||= end_of_association_chain.ordered
    end

    def set_active_item
      @active = :checks
    end
end
