class RoomsController < InheritedResources::Base
  filter_resource_access
  custom_actions :resource => :info
  layout 'print', only: :info
  
  def show
    @room        = Room.includes(:computer, :board, :projector).find( params[:id] )
    @boardchecks = @room.boardchecks.ordered
    @computer    = @room.computer
    @board       = @room.board
    @projector   = @room.projector
    
    show!
  end

  def info
    @url = new_quest_url( room: resource.name, host: BoardManager::Config.host )
    
    info!
  end

  def edit
    @room = Room.includes(:computer, :board, :projector).find( params[:id] )
    @room.build_computer unless @room.computer.present?
    @room.build_board unless @room.board.present?
    @room.build_projector unless @room.projector.present?
    
    edit!
  end
  
  protected
    def collection
      @rooms ||= end_of_association_chain.ordered
    end

    def set_active_item
      @active = :rooms
    end
end
