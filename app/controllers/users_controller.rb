# encoding: UTF-8
class UsersController < InheritedResources::Base
  filter_access_to :all
  filter_access_to :create, attribute_check: false

  def show
    @rooms  = resource.rooms.ordered
    @quests = resource.quests.ordered
    @quests += resource.open_quests.ordered

    load_duties
  end
  
  def create
    @user = User.new
    @user.assign_attributes( params[:user], :as => as_role[:as] )
    @user.password = @user.password_confirmation = User.create_password
    create!
  end

  def edit
    resource.sponsorships.build
  end
  

  def update
    @user = User.find params[:id]
    @user.assign_attributes( params[:user], :as => as_role[:as] )
    if @user.level_changed? && @user.level > @user.level_was
      UserMailer.level_upgraded(@user, @user.level_was).deliver
    end

    update!
  end

  def tweet
    @user   = User.find params[:id]
    twitter = @user.twitter_name
    message = "MSO Tafelrunde Testtweet #{l Time.now}"

    begin
      if Rails.env.production?
        Twitter.direct_message_create( twitter, message )
      else
        # Hack to avoid Twitter timestamp problems
        Timecop.return 
        Twitter.direct_message_create( twitter, message )
        Timecop.travel( Time.parse( BoardManager::Config.time ) )
      end
      
      flash[:notice] = "Erfolgreich an #{twitter} gezwitschert!"
    rescue Exception => e
      flash[:error] = "Beim Zwitschern an ‘#{twitter}’ ist etwas schief gegangen. Stelle sicher, dass ‘#{twitter}’ ‘#{ BoardManager::Config.twitter['name'] }’ folgt.<br>Ursprüngliche Fehlermeldung: “#{e.message}”".html_safe
    end

    redirect_to :back
  end
  
  protected
    def collection
      @users ||= end_of_association_chain.includes(:duties).ordered
    end

    def role_given?
      permitted_to?(:update, :users)
    end
        
    def as_role
      { as: permitted_to?(:update, :users) ? :admin : :default }
    end

    def set_active_item
      @active = (current_user && current_user.id == params[:user_id].to_i) ? :user : :users
    end

    def load_duties
      @timetable = Array.new( 8 ) { Array.new(6, nil) }
      @duties = @user.duties.ordered
      @duties.each { |d| @timetable[ d.period.number ][ d.period.day ] = d.room }
    end
end
