class BoardsController < InheritedResources::Base
  filter_resource_access

  protected
    def collection
      @boards ||= end_of_association_chain.ordered
    end
end
