class ProjectorsController < InheritedResources::Base
  filter_resource_access
  
  protected
    def collection
      @projectors ||= end_of_association_chain.ordered
    end
end
