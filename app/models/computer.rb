class Computer < ActiveRecord::Base
  belongs_to :room

  before_validation :format_macs, only: [:mac_en_address, :mac_wifi_address]
  
  attr_accessible :room_id, :mac_en_address, :mac_wifi_address, :model, :note, :purchased_on, :serial, :kind, :ip
  
  validates :room, :presence => true
  
  validates :ip, :format => { :with => /^10\.\d{1,3}\.\d{1,3}\.\d{1,3}$/,
      :message => "bitte in der Form 10.1.23.234 angeben." }, :uniqueness => true, :allow_blank => true
  
  validates :mac_en_address, :format => { :with => /^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$/,
      :message => "bitte in der Form AA:BB:CC:DD:EE:FF angeben." }, :uniqueness => true, :allow_blank => true
  
  validates :mac_wifi_address, :format => { :with => /^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$/,
      :message => "bitte in der Form AA:BB:CC:DD:EE:FF angeben." }, :uniqueness => true, :allow_blank => true
      
  def format_macs
    self.mac_en_address.upcase!
    self.mac_wifi_address.upcase!
  end
end
