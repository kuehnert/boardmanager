class UserObserver < ActiveRecord::Observer
  def after_create(user)
    UserMailer.user_created(user).deliver
  end
end
