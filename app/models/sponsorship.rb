class Sponsorship < ActiveRecord::Base
  belongs_to :user
  belongs_to :room

  attr_accessible :user_id, :room_id

  validates :room_id, presence: true
  validates :user_id, presence: true
end
