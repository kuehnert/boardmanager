class Board < ActiveRecord::Base
  belongs_to :room
  attr_accessible :room_id, :note, :purchased_on, :serial, :manufacturer, :model, :firmware

  validates :room_id, presence: true, uniqueness: true
  
  scope :ordered, includes(:room).order('rooms.name')
end
