class Period < ActiveRecord::Base
  DAYS = [ "Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag" ]

  has_many :duties, dependent: :destroy
  has_many :users, through: :duties
  has_many :quests, dependent: :destroy
  attr_accessible :day, :finish, :number, :start
  
  validates :day, :presence => true, :uniqueness => { scope: :number }
  validates :number, :presence => true
  validates :start, :presence => true
  validates :finish, :presence => true

  def self.current
    time = Time.now.change(month: 1, day: 1, year: 2000)
    Period.where("day=? AND start <= ? AND finish > ?", Date.today.wday, time, time).first
  end
  
  def number_human
    "#{number + 1}."
  end
  
  def name
    "#{ Period::DAYS[day] } #{ number_human }&nbsp;Stunde".html_safe
  end
end
