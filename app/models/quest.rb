class Quest < ActiveRecord::Base
  belongs_to :room
  belongs_to :period
  belongs_to :user

  attr_accessible :note, :problem, :solved, :room_id, :period_id, :user_id
  
  validates :room_id, :presence => true
  # validates :period_id, :presence => true
  validates_with QuestUniquenessValidator
  
  scope :ordered, order("solved, created_at DESC")
  scope :solved, where(solved: true)
end
