class Projector < ActiveRecord::Base
  belongs_to :room
  attr_accessible :manufacturer, :model, :resolution, :serial, :room_id

  validates :room_id, presence: true, uniqueness: true
  validates :resolution, :format => { :with => /^\d{3,4}x\d{3,4}$/ }

  scope :ordered, includes(:room).order('rooms.name')
end
