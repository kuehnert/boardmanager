class Boardcheck < ActiveRecord::Base
  STATES = { 0 => 'Stand-By', 1 => 'An', 2 => "Aus" }
  POINTS = [:undamaged, :timetable_present, :speakers_on, :speakers_volume_set, :projector_remote_ok, :projector_source_set, :keyboard_ok, :teacher_pen_ok, :student_pen_ok, :sharpness_set, :guest_account_ok, :network_accounts_ok, :activdriver_ok, :calibrated, :speakers_driver_ok, :updates_loaded]
  
  belongs_to :room
  belongs_to :user

  validates :room_id, presence: true
  validates :computer_state, presence: true

  scope :ordered, order('created_at desc')

  attr_accessible :room_id, :activdriver_ok, :calibrated, :computer_state, :guest_account_ok, :issues_present, :keyboard_ok, :user_id, :network_accounts_ok, :note, :projector_remote_ok, :projector_source_set, :image_aligned, :sharpness_set, :speakers_driver_ok, :speakers_on, :speakers_volume_set, :student_pen_ok, :teacher_pen_ok, :timetable_present, :undamaged, :updates_loaded

  def self.latest
    ordered.first    
  end
  
  def computer_state_s
    STATES[ computer_state ]
  end
  
  def issues
    issues = POINTS.select { |p| ! send(p) }
    issues << :computer_state if computer_state == 2
    issues.map { |i| "* " + Boardcheck.human_attribute_name(i) }.join("\n")
  end
end
