# encoding: UTF-8
class User < ActiveRecord::Base
  CHARACTERS = ('A'..'Z').to_a + ('a'..'z').to_a + ('0'..'9').to_a
  LEVELS     = ["Knappe", "Ritter", "Fürst", "Herzog", "König"]
  MAX_LEVEL  = LEVELS.size - 1
  
  def self.create_password
    Array.new(8) { CHARACTERS.sample }.join
  end
  
  # Include default devise modules. Others available are: :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :token_authenticatable, :confirmable, :lockable, :timeoutable

  has_many :boardchecks, dependent: :nullify
  has_many :duties, dependent: :destroy
  has_many :quests, dependent: :destroy
  has_many :sponsorships, dependent: :destroy
  has_many :rooms, :through => :sponsorships
  has_many :open_quests, :through => :rooms, :source => :quests
  
  scope :ordered, order(:year, :form, :lastname, :firstname)
  scope :admins, where(level: MAX_LEVEL)
  scope :twitterers, where("twitter_name <> ''")
  scope :by_level, order("level desc")
  
  # Setup accessible (or protected) attributes for your model
  accepts_nested_attributes_for :sponsorships, :reject_if => lambda { |a| a[:room_id].blank? }, :allow_destroy => true
  attr_accessible :firstname, :lastname, :year, :form, :email, :twitter_name, :password,
    :password_confirmation, :remember_me
  attr_accessible :firstname, :lastname, :year, :form, :email, :twitter_name, :password,
    :password_confirmation, :remember_me, :level, :sponsorships_attributes, :room_ids, as: :admin

  validates :firstname, presence: true
  validates :lastname, presence: true
  validates :email, presence: true, uniqueness: true
  
  def yearform
    year.present? ? "#{year}#{form}" : '-'
  end

  def level_s
    LEVELS[level]
  end
    
  def role_symbols
    [ level_s.parameterize.underscore.to_sym ]
  end
  
  def titled
    "#{level_s} #{firstname} (#{yearform})"
  end
  
  def name
    titled
  end
  
  def twitters?
    twitter_name.present?
  end
end
