class Duty < ActiveRecord::Base
  belongs_to :user
  belongs_to :period

  attr_accessible :user_id, :period_id, :room

  validates :user_id, :presence => true
  validates :period_id, :presence => true, :uniqueness => { scope: :user_id }
  validates :room, :presence => true
  
  scope :ordered, includes(:period, :user).order( "periods.day, periods.number, users.level DESC" )
end
