class Room < ActiveRecord::Base
  BUILDINGS        = { 0 => 'Altbau', 1 => 'Neubau' }
  LEVELS           = { 0 => 'Erdgeschoss', 1 => '1. Stock', 2 => '2. Stock', 3 => '3. Stock' }
  MAX_PENALTY      = 2000
  BUILDING_PENALTY = 150
  LEVEL_PENALTY    = 300

  has_one :computer, dependent: :nullify
  has_one :board, dependent: :nullify
  has_one :projector, dependent: :nullify
  has_many :boardchecks, dependent: :destroy
  has_many :quests, dependent: :destroy
  has_many :sponsorships, dependent: :destroy
  has_many :users, :through => :sponsorships
  
  accepts_nested_attributes_for :computer, :allow_destroy => true
  accepts_nested_attributes_for :board, :allow_destroy => true
  accepts_nested_attributes_for :projector, :allow_destroy => true
  
  validates :building, :presence => true
  validates :level, :presence => true
  validates :name, :presence => true, :uniqueness => true
  
  attr_accessible :building, :level, :name, :computer_attributes, :board_attributes, :projector_attributes
  
  scope :ordered, order(:building, :level, :name)

  def self.named(name)
    Room.where( 'name like ?', "%#{name[/\d+$/]}%").first rescue nil
  end
  
  def building_s
    BUILDINGS[ building ]
  end
  
  def level_s
    LEVELS[ level ]
  end
  
  def last_check_ok?
    check = boardchecks.latest
    return nil unless check.present? 
    return ! check.issues_present?
  end
  
  def number
    name[ /\d+$/ ].to_i
  end
  
  def distance(other)
    Room.distance( self.name, other.is_a?(Room) ? other.name : other )
  end
  
  protected
    def self.distance(r1, r2)
      r1 = Room.room_by_name(r1)
      r2 = Room.room_by_name(r2)
      return MAX_PENALTY if r1.building == 2 || r1.building == 2
      
      penalty = 0
      penalty += BUILDING_PENALTY if r1.building != r2.building
      penalty += LEVEL_PENALTY * (r1.level - r2.level).abs
      penalty += (r1.number % 100 - r2.number % 100).abs ** 2
      
      return penalty
    end
    
    def self.room_by_name(name)
      room = Room.new( name: name)
      if (number = name[ /\d+$/ ].to_i) > 0
        room.building = number / 400
        room.level    = number % 400 / 100
      else
        room.building = 2
        room.level    = 4
      end
      
      return room
    end
end
