class UserMailer < ActionMailer::Base
  helper :application, :users
  
  default from: "MSO nicht antworten <no-reply@marienschule.com>"

  def quest_created(user, quest)
    @user  = user
    @quest = quest
    mail to: @user.email, subject: "Tafelritter: Hilferuf in #{quest.room.name} (#{l quest.created_at, format: :timeonly})!".upcase
  end

  def quest_accepted(user, quest)
    @user  = user
    @quest = quest
    mail to: @user.email, subject: "Erledigt: Hilferuf in #{quest.room.name} (#{l quest.created_at, format: :timeonly})"
  end

  def level_upgraded(user, level_was)
    @user = user
    @old_title = User::LEVELS[level_was]
    mail to: @user.email, subject: "Erhebung in den Ritterstand"
  end
  
  def user_created(user)
    @user = user
    recipients = User.admins.map(&:email)
    subject = "Neuer Tafel#{user.level_s.downcase} #{user.firstname}"
    subject += " (#{user.yearform})" if user.year.present?
    mail to: recipients, subject: subject
  end
end
