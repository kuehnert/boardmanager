BoardManager::Application.routes.draw do
  devise_for :users, :path_prefix => 'auth'

  resource :knighttable, only: :show
  resources :boardchecks
  resources :boards
  resources :computers
  resources :projectors

  resources :quests do
    post :accept, on: :member
  end
  
  resources :rooms do
    get :info, on: :member
  end
  
  resources :users do
    resources :duties, only: :index do
      get :edit_all, on: :collection
      post :update_all, on: :collection
    end
    
    post :tweet, on: :member
  end

  get "qrcode" => "qrcodes#show"
  root :to => 'knighttables#show'
end
