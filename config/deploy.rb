require "bundler/capistrano"

set :user,        "mksadmin"
set :application, "boardmanager"
set :repository,  "git@bitbucket.org:kuehnert/boardmanager.git"
set :deploy_to,   "/Library/Server/Web/Data/RailsApps/#{application}"
set :scm, :git
set :use_sudo, false

role :web, "neumann.marienschule.com"
role :app, "neumann.marienschule.com"
role :db,  "neumann.marienschule.com", :primary => true

after "deploy:finalize_update", "deploy:symlink_config"
after "deploy:restart", "deploy:cleanup"

namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end

  desc "Make symlink for database yaml & app config" 
  task :symlink_config do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml" 
    run "ln -nfs #{shared_path}/config/application.yml #{release_path}/config/application.yml"
  end
end
