ActionMailer::Base.smtp_settings = {
  address:              BoardManager::Config.mailer['server'],
  port:                 BoardManager::Config.mailer['port'],
  domain:               BoardManager::Config.mailer['domain'],
  user_name:            BoardManager::Config.mailer['user'],
  password:             BoardManager::Config.mailer['password'],
  authentication:       "plain",
  enable_starttls_auto: true
}

if Rails.env.development?
  require 'development_mail_interceptor'
  Mail.register_interceptor(DevelopmentMailInterceptor) 
end
