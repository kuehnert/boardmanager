Twitter.configure do |config|
  config.consumer_key       = BoardManager::Config.twitter['consumer_key']
  config.consumer_secret    = BoardManager::Config.twitter['consumer_secret']
  config.oauth_token        = BoardManager::Config.twitter['oauth_token']
  config.oauth_token_secret = BoardManager::Config.twitter['oauth_token_secret']
end
