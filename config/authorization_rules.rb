authorization do
  role :guest do
    has_permission_on :knighttables, to: :read
    has_permission_on :quests, to: :create
    has_permission_on :rooms, to: :index
    has_permission_on :users, to: :index
    # has_permission_on :devise_sessions, to: :manage
    # has_permission_on :devise_registrations, to: :manage
    # has_permission_on :devise_confirmations, to: :manage
  end
  
  role :knappe do
    includes :guest
    has_permission_on :boardchecks, to: :read
    has_permission_on :boards,      to: :index
    has_permission_on :computers,   to: :index
    has_permission_on :projectors,  to: :index
    has_permission_on :users,       to: :index
    has_permission_on :users,       to: :tweet do
      if_attribute :id => is { user.id }
    end
  end
  
  role :ritter do
    includes :knappe
    has_permission_on :boardchecks, to: :create
    has_permission_on :boards,      to: :read
    has_permission_on :computers,   to: :read
    has_permission_on :projectors,  to: :read
    has_permission_on :rooms,       to: :read

    has_permission_on :duties, to: :manage do
      if_attribute :user => is { user }
    end

    has_permission_on :quests, to: :read
    has_permission_on :quests, to: :accept do
      if_attribute :user => is { nil }
    end 
    has_permission_on :quests, to: [:update] do
      if_attribute :user => is { user }
    end 

    has_permission_on :users, to: :read
  end
  
  role :furst do
    includes :ritter
    has_permission_on :boardchecks, to: :manage
    has_permission_on :boards,      to: [:create, :update]
    has_permission_on :computers,   to: [:create, :update]
    has_permission_on :projectors,  to: [:create, :update]
    has_permission_on :rooms,       to: :update
  end
  
  role :herzog do
    includes :furst
    has_permission_on :boards,      to: :manage
    has_permission_on :computers,   to: :manage
    has_permission_on :projectors,  to: :manage
    has_permission_on :rooms,       to: :manage
    has_permission_on :duties,      to: :manage
  end
  
  role :konig do
    has_omnipotence
  end
end

privileges do
  privilege :read, :rooms, includes: :info
  privilege :edit, :duties, includes: :edit_all
  privilege :update, :duties, includes: :update_all
  
  privilege :manage, includes: [:create, :read, :update, :delete]
  privilege :read,   includes: [:index, :show]
  privilege :create, includes: :new
  privilege :update, includes: :edit
  privilege :delete, includes: :destroy
end
